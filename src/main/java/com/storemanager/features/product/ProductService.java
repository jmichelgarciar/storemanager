package com.storemanager.features.product;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * TODO: add quick doc about this file
 */
public interface ProductService {

    List<Product> getProductsList();

    Product getProduct(final long id);

    Product addProduct(@NotNull final Product product);

    Product deleteProduct(final long id);

}
