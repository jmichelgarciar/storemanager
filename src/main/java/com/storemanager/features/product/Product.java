package com.storemanager.features.product;

/**
 * TODO: add quick doc about this file
 */
public class Product
{
    private long id;

    private String name             = null;
    private String description      = null;
    private String shortDescription = null;
    private Long   typeId           = null;
    private Long   measureUnitId    = null;

    public Product(final String name, final Long typeId, final Long measureUnitId, final String description,
                   final String shortDescription)
    {
        this.name = name;
        this.typeId = typeId;
        this.measureUnitId = measureUnitId;
        this.description = description;
        this.shortDescription = shortDescription;
    }

    public Product()
    {
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getTypeId()
    {
        return typeId;
    }

    public void setTypeId(Long typeId)
    {
        this.typeId = typeId;
    }

    public Long getMeasureUnitId()
    {
        return measureUnitId;
    }

    public void setMeasureUnitId(Long measureUnitId)
    {
        this.measureUnitId = measureUnitId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription)
    {
        this.shortDescription = shortDescription;
    }

    @Override
    public String toString()
    {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", typeId=" + typeId +
                ", measureUnitId=" + measureUnitId +
                '}';
    }
}
