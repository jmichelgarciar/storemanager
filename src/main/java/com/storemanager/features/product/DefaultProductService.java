package com.storemanager.features.product;

import com.storemanager.main.persistence.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * TODO: add quick doc about this file
 */
@Service
public class DefaultProductService implements ProductService
{

    @Autowired
    ProductMapper productMapper;

    public Product addProduct(@NotNull final Product product)
    {
        productMapper.save(product);

        return product;
    }

    public List<Product> getProductsList()
    {
        return productMapper.findAll();
    }

    public Product getProduct(@NotNull final long id)
    {
        return productMapper.findById(id);
    }

    public Product deleteProduct(final long id)
    {
        Product product = productMapper.findById(id);
        productMapper.deleteById(id);

        return product;
    }
}
