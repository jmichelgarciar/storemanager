package com.storemanager.features.attribute;

import com.storemanager.features.product.Product;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * TODO: add quick doc about this file
 */
public interface AttributeService
{

    MeasureUnit addMeasureUnit(@NotNull final MeasureUnit measureUnit);

    Type addType(Type type);

    List<MeasureUnit> findAllMeasureUnits();

    List<Type> findAllTypes();

    MeasureUnit findMeasureUnitById(final long id);

    Type findTypeById(final long id);

    MeasureUnit deleteMeasureUnit(long id);

    Type deleteType(long id);
}
