package com.storemanager.features.attribute;

/**
 * measure unit, used as a attribute for other features
 */
public class MeasureUnit
{
    private long id;

    private String name;
    private String shortDescription;
    private String description;

    public MeasureUnit(String name, String short_description, String description)
    {
        this.name = name;
        this.shortDescription = short_description;
        this.description = description;
    }

    public MeasureUnit()
    {
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void setShortDescription(String short_description)
    {
        this.shortDescription = short_description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return "MeasureUnit{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
