package com.storemanager.features.attribute;

import com.storemanager.main.persistence.AttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * TODO: add quick doc about this file
 */
@Service
public class DefaultAttributeService implements AttributeService
{

    @Autowired
    AttributeMapper attributeMapper;

    public MeasureUnit addMeasureUnit(@NotNull final MeasureUnit measureUnit)
    {
        attributeMapper.saveMeasureUnit(measureUnit);
        return measureUnit;
    }

    @Override
    public Type addType(Type type)
    {
        attributeMapper.saveType(type);
        return type;
    }

    @Override
    public List<MeasureUnit> findAllMeasureUnits()
    {
        return attributeMapper.findAllMeasureUnits();
    }

    @Override
    public List<Type> findAllTypes()
    {
        return attributeMapper.findAllTypes();
    }

    @Override
    public MeasureUnit findMeasureUnitById(final long id)
    {
        MeasureUnit measureUnitById = attributeMapper.findMeasureUnitById(id);
        return measureUnitById;
    }

    @Override
    public Type findTypeById(final long id)
    {
        return attributeMapper.findTypeById(id);
    }

    @Override
    public MeasureUnit deleteMeasureUnit(long id)
    {
        MeasureUnit measureUnit = attributeMapper.findMeasureUnitById(id);
        attributeMapper.deleteMeasureUnit(id);

        return measureUnit;
    }

    @Override
    public Type deleteType(long id)
    {
        Type type = attributeMapper.findTypeById(id);
        attributeMapper.deleteType(id);

        return type;
    }

}
