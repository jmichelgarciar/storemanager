package com.storemanager.main.http.v1;

import com.storemanager.main.i18n.LocaleMessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * This controller gives basic info about the product
 * for simple checks if the server is running, use docs, etc.
 */
@RestController
@RequestMapping
public class MainControllerV1
{
    @Autowired
    LocaleMessageService messageService;

    @ApiOperation(value = HttpApiConstV1.V1_STATUS, notes = "check the status of the application, if it is running")
    @RequestMapping(value = HttpApiConstV1.V1_STATUS,
            method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE},
            produces = "text/plain")
    @ResponseBody
    public String getStatus()
    {
        return messageService.getMessage("v1.status");
    }

    @ApiOperation(value = HttpApiConstV1.V1_SWAGGER, notes = "redirect to swagger ui")
    @RequestMapping(value = HttpApiConstV1.V1_SWAGGER,
            method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
    @ResponseBody
    public RedirectView getDocs()
//        TODO, leave for these attributes, for later reference
//    public RedirectView getDocs(RedirectAttributes redirectAttributes)
    {
//        redirectAttributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
//        redirectAttributes.addAttribute("attribute", "redirectWithRedirectView");
        return new RedirectView("/swagger-ui.html");
    }

}
