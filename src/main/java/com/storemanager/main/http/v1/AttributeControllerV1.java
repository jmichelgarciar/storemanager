package com.storemanager.main.http.v1;

import com.storemanager.features.attribute.AttributeService;
import com.storemanager.features.attribute.MeasureUnit;
import com.storemanager.features.attribute.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Controller to interact with Products feature
 */
@RestController
@RequestMapping
public class AttributeControllerV1
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AttributeControllerV1.class);

    @Autowired
    AttributeService attributeService;

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_MEASUREUNIT, method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public MeasureUnit addAttribute(@RequestBody final @NotNull MeasureUnit measureUnit)
    {
        return attributeService.addMeasureUnit(measureUnit);
    }

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_TYPE, method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Type addAttribute(@RequestBody final @NotNull Type type)
    {
        return attributeService.addType(type);
    }

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_MEASUREUNIT + "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public MeasureUnit getMeasureUnit(@PathVariable long id)
    {
        return attributeService.findMeasureUnitById(id);
    }

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_TYPE + "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Type getType(@PathVariable long id)
    {
        return attributeService.findTypeById(id);
    }



    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_MEASUREUNIT_LIST, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<MeasureUnit> findAllMeasureUnits()
    {
        return attributeService.findAllMeasureUnits();
    }

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_TYPE_LIST, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Type> findAllTypes()
    {
        return attributeService.findAllTypes();
    }



    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_MEASUREUNIT + "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public MeasureUnit deleteMeasureUnit(final @PathVariable long id)
    {
        return attributeService.deleteMeasureUnit(id);
    }

    @RequestMapping(value = HttpApiConstV1.V1_ATTRIBUTE_TYPE + "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Type deleteType(final @PathVariable long id)
    {
        return attributeService.deleteType(id);
    }

    // Convert a predefined exception to an HTTP Status code
    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Internal error occurred while executing the request")  // 409
    @ExceptionHandler(RuntimeException.class)
    public void conflict(Exception exception) {
        LOGGER.error("Internal error occurred while executing the request");
        LOGGER.error(" --> exception: " + exception.getMessage());
        LOGGER.error(" --> cause: " + (exception.getCause() != null ? exception.getCause().getMessage() : "no details!"));
    }

}
