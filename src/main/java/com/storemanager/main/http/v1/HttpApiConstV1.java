package com.storemanager.main.http.v1;

/**
 * rodislav
 * 20.02.16.
 */
public class HttpApiConstV1
{
    //    Main
    public static final String V1_STATUS  = "/v1/status";
    public static final String V1_SWAGGER = "/v1/swagger";

    //    Product
    public static final String V1_PRODUCT      = "/v1/product";
    public static final String V1_PRODUCT_LIST = "/v1/product/list";

    //    Attribute
    public static final String V1_ATTRIBUTE_MEASUREUNIT      = "/v1/attribute/measureunit";
    public static final String V1_ATTRIBUTE_MEASUREUNIT_LIST = "/v1/attribute/measureunit/list";

    public static final String V1_ATTRIBUTE_TYPE      = "/v1/attribute/type";
    public static final String V1_ATTRIBUTE_TYPE_LIST = "/v1/attribute/type/list";
}
