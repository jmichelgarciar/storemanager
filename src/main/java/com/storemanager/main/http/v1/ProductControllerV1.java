package com.storemanager.main.http.v1;

import com.storemanager.features.product.Product;
import com.storemanager.features.product.ProductService;
import com.storemanager.main.http.v1.HttpApiConstV1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Controller to interact with Products feature
 */
@RestController
@RequestMapping
public class ProductControllerV1
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductControllerV1.class);

    @Autowired
    ProductService productService;

    @RequestMapping(value = HttpApiConstV1.V1_PRODUCT, method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public Product addProduct(@RequestBody final @NotNull Product product)
    {
        return productService.addProduct(product);
    }

    @RequestMapping(value = HttpApiConstV1.V1_PRODUCT + "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Product getProduct(@PathVariable long id)
    {
        return productService.getProduct(id);
    }

    @RequestMapping(value = HttpApiConstV1.V1_PRODUCT_LIST, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Product> getList()
    {
        return productService.getProductsList();
    }

    @RequestMapping(value = HttpApiConstV1.V1_PRODUCT + "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Product deleteProduct(final @PathVariable long id)
    {
        return productService.deleteProduct(id);
    }

    // Convert a predefined exception to an HTTP Status code
    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Internal error occurred while executing the request")  // 409
    @ExceptionHandler(RuntimeException.class)
    public void conflict(Exception exception) {
        LOGGER.error("Internal error occurred while executing the request");
        LOGGER.error(" --> exception: " + exception.getMessage());
        LOGGER.error(" --> cause: " + (exception.getCause() != null ? exception.getCause().getMessage() : "no details!"));
    }

}
