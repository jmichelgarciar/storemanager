package com.storemanager.main.http.v2;

import com.storemanager.main.i18n.LocaleMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * TODO: add quick doc about this file
 */
@RestController
@RequestMapping("/v2")
public class MainControllerV2
{
    @Autowired
    LocaleMessageService messageService;

    @RequestMapping(value = "status",
            method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE},
            produces = "text/plain")
    @ResponseBody
    public String getStatus()
    {
        return messageService.getMessage("v1.status");
    }

    @RequestMapping(value = "api", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE},
            produces = "text/plain")
    @ResponseBody
    public RedirectView getDocs(RedirectAttributes redirectAttributes)
    {
//        redirectAttributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
//        redirectAttributes.addAttribute("attribute", "redirectWithRedirectView");
        return new RedirectView("/swagger-ui.html");
    }

}
