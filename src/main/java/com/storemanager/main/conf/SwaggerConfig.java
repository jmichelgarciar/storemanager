package com.storemanager.main.conf;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@EnableAutoConfiguration
public class SwaggerConfig
{

    @Bean
    public Docket v1Api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v1")
                .apiInfo(apiInfo("v1"))
                .select()
                .paths(regex("/v1.*"))
                .build();
    }

    @Bean
    public Docket v2Api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v2")
                .apiInfo(apiInfo("v2"))
                .select()
                .paths(regex("/v2.*"))
                .build();
    }

    private ApiInfo apiInfo(final String version) {
        return new ApiInfoBuilder()
                .title("StoreManager / Swagger UI")
                .description("Documentation for the StoreManager public RESTful API")
                .termsOfServiceUrl("")
                .contact("Rodislav Moldovan <rodislav@gmail.com>")
                .license("Creative Commons")
                .licenseUrl("")
                .version(version)
                .build();
    }
}