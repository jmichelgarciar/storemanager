package com.storemanager.main.i18n;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * rodislav
 * 17.02.16.
 */
@Component
public class LocaleMessageServiceBean implements LocaleMessageService
{

    @Autowired
    MessageSource messageSource;

    @Override
    public String getMessage(String keyName)
    {
//        do not delete: use for demo purpose
        Locale locale = LocaleContextHolder.getLocale();
//        Locale locale = Locale.US;
        return messageSource.getMessage(keyName, null, locale);
    }
}
