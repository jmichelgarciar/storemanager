package com.storemanager.main.i18n;

/**
 * rodislav
 * 17.02.16.
 */
public interface LocaleMessageService
{
    String getMessage(String keyName);
}
