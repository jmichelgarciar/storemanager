package com.storemanager.main.persistence;

import com.storemanager.features.attribute.MeasureUnit;
import com.storemanager.features.attribute.Type;
import org.apache.ibatis.annotations.*;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface AttributeMapper
{

//    INSERT

    @InsertProvider(type = AttributeSQLProvider.class, method = "saveMeasureUnit")
    @Options(useGeneratedKeys = true)
    int saveMeasureUnit(final MeasureUnit measureUnit);

    @InsertProvider(type = AttributeSQLProvider.class, method = "saveType")
    @Options(useGeneratedKeys = true)
    int saveType(final Type type);

//    SELECT

    @SelectProvider(type = AttributeSQLProvider.class, method = "findMeasureUnitById")
    @Result(javaType = MeasureUnit.class)
    MeasureUnit findMeasureUnitById(@NotNull final long id);

    @SelectProvider(type = AttributeSQLProvider.class, method = "findTypeById")
    @Result(javaType = Type.class)
    Type findTypeById(@NotNull final long id);

    @SelectProvider(type = AttributeSQLProvider.class, method = "findAllMeasureUnits")
    @Results
    @Result(javaType = MeasureUnit.class)
    List<MeasureUnit> findAllMeasureUnits();

    @SelectProvider(type = AttributeSQLProvider.class, method = "findAllTypes")
    @Results
    @Result(javaType = Type.class)
    List<Type> findAllTypes();

//    DELETE

    @DeleteProvider(type = AttributeSQLProvider.class, method = "deleteMeasureUnitById")
    void deleteMeasureUnit(final long id);

    @DeleteProvider(type = AttributeSQLProvider.class, method = "deleteTypeById")
    void deleteType(final long id);
}
