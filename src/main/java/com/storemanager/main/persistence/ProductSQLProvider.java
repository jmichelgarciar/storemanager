package com.storemanager.main.persistence;

import org.apache.ibatis.jdbc.SQL;

/**
 * rodislav
 * 02.02.16.
 */
@SuppressWarnings("unused")
public class ProductSQLProvider
{
    public String findAll() {
        return new SQL()
                .SELECT("*")
                .FROM("PRODUCT")
                .toString();
    }

    public String findById() {
        return new SQL()
                .SELECT("*")
                .FROM("PRODUCT")
                .WHERE("id = #{id}")
                .toString();
    }

    public String save() {
        return new SQL()
                .INSERT_INTO("PRODUCT")
                .VALUES("NAME, DESCRIPTION, SHORT_DESCRIPTION, TYPE_ID, MEASURE_UNIT_ID",
                        "#{name}, #{description}, #{shortDescription}, #{typeId}, #{measureUnitId}")
                .toString();
    }

    public String deleteById()
    {
        return new SQL()
                .DELETE_FROM("PRODUCT")
                .WHERE("id = #{id}")
                .toString();
    }
}
