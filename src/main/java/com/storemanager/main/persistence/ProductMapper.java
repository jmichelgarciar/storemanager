package com.storemanager.main.persistence;

import com.storemanager.features.product.Product;
import org.apache.ibatis.annotations.*;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ProductMapper
{

//    SELECT

    @SelectProvider(type = ProductSQLProvider.class, method = "findById")
    @Result(javaType = Product.class)
    Product findById(@NotNull final long id);

    @SelectProvider(type = ProductSQLProvider.class, method = "findAll")
    @Results
    @Result(javaType = Product.class)
    List<Product> findAll();

//    INSERT

    @InsertProvider(type = ProductSQLProvider.class, method = "save")
    @Options(useGeneratedKeys = true)
    int save(Product product);

    @DeleteProvider(type = ProductSQLProvider.class, method = "deleteById")
    void deleteById(final long id);
}
