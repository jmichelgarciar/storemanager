package com.storemanager.main.persistence;

import org.apache.ibatis.jdbc.SQL;

/**
 * rodislav
 * 02.02.16.
 */
@SuppressWarnings("unused")
public class AttributeSQLProvider
{
    public static String[] CEVA = new String[1];

    public String saveMeasureUnit() {
        return new SQL()
                .INSERT_INTO("MEASURE_UNIT")
                .VALUES("NAME, DESCRIPTION, SHORT_DESCRIPTION",
                        "#{name}, #{description}, #{shortDescription}")
                .toString();
    }

    public String saveType() {
        return new SQL()
                .INSERT_INTO("TYPE")
                .VALUES("NAME, DESCRIPTION, SHORT_DESCRIPTION",
                        "#{name}, #{description}, #{shortDescription}")
                .toString();
    }

    public String findMeasureUnitById() {
        return new SQL()
                .SELECT("*")
                .FROM("MEASURE_UNIT")
                .WHERE("id = #{id}")
                .toString();
    }

    public String findTypeById() {
        return new SQL()
                .SELECT("*")
                .FROM("TYPE")
                .WHERE("id = #{id}")
                .toString();
    }

    public String findAllMeasureUnits() {
        return new SQL()
                .SELECT("*")
                .FROM("MEASURE_UNIT")
                .toString();
    }

    public String findAllTypes() {
        return new SQL()
                .SELECT("*")
                .FROM("TYPE")
                .toString();
    }

    public String deleteMeasureUnitById() {
        return new SQL()
                .DELETE_FROM("MEASURE_UNIT")
                .WHERE("id = #{id}")
                .toString();
    }

    public String deleteTypeById() {
        return new SQL()
                .DELETE_FROM("TYPE")
                .WHERE("id = #{id}")
                .toString();
    }
}
