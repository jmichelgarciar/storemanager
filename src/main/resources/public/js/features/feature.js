var smFeature = function(name_)
{
    this.name = name_;
    this.$mainUi = {};

    this.self = this;

    this.execute = function(command, jsObject)
    {
        return this.self[command](jsObject);
    };
};

var extendFeature = function(name)
{
    return new function()
    {
        var ceva = new smFeature(name);

        for(var i in ceva)
        {
            this[i] = ceva[i];
        }

        this.self = this;
    };
};