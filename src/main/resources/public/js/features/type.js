var smType = extendFeature("type");

smType.$list = {};
smType.$list_template = {};
smType.$tab = {};

smType.list = function(data)
{
    $.get('http://localhost:9090/v1/attribute/type/list')
        .done(function (data)
        {
            smType.$list.html(smFragments.dataToHtml(data, smType._list_template));
        })
        .fail(function (data)
        {
            smConsole("fail :: " + data);
        });

    smType.focus();

    return true;
};

smType.initUi = function()
{
    smType.$mainUi = $('#type');
    smType.$list = smType.$mainUi.find('ul');
    smType._list_template = smType.$list.find('.template').removeClass('template').remove()[0].outerHTML;

    smType.$tab = $('#mainTabs').find('a[href="#type"]');
};


smType.focus = function()
{
    smType.$tab.tab('show')
};