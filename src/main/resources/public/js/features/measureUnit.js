var smMeasureUnit = extendFeature("measureunit");

smMeasureUnit.$list = {};
smMeasureUnit.$list_template = {};
smMeasureUnit.$tab = {};

smMeasureUnit.list = function(data)
{
    $.get('http://localhost:9090/v1/attribute/measureunit/list')
        .done(function (data)
        {
            smMeasureUnit.$list.html(smFragments.dataToHtml(data, smMeasureUnit._list_template));
        })
        .fail(function (data)
        {
            smConsole("fail :: " + data);
        });

    smMeasureUnit.focus();

    return true;
};

smMeasureUnit.initUi = function()
{
    smMeasureUnit.$mainUi = $('#measureUnit');
    smMeasureUnit.$list = smMeasureUnit.$mainUi.find('ul');
    smMeasureUnit._list_template = smMeasureUnit.$list.find('.template').removeClass('template').remove()[0].outerHTML;

    smMeasureUnit.$tab = $('#mainTabs').find('a[href="#measureUnit"]');
};


smMeasureUnit.focus = function()
{
    smMeasureUnit.$tab.tab('show')
};