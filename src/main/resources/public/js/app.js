$(
    function ()
    {
        smFeatures
            .register(smMeasureUnit)
            .register(smType);

        smFragments
            .loadFragments()
            .whenReady(
                function ()
                {
                    smMeasureUnit.initUi();
                    smType.initUi();

                    smRouter.parseUri();
                });
    }
);
