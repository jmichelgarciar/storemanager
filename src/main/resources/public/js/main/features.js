var smFeatures = new function() {
    this.resources = {};

    this.NOT_EXECUTED = ".::NOT_EXECUTED::." + Math.floor(Date.now() / 1000);
};

/**
 * execute a business command of the resource
 * @param resourceName name of the resource to execute on
 * @param command the name of the command, actually is name of the function from public api
 * @param jsObject the js-data-object which contains all needed data for the resource to execute the requested command
 * @returns {*} result of the operation or smFeatures.NOT_EXECUTED
 */
smFeatures.execute = function(resourceName, command, jsObject)
{
    if(typeof smFeatures.resources[resourceName] != "undefined")
    {
        return smFeatures.resources[resourceName].execute(command, jsObject);
    }

    return smFeatures.NOT_EXECUTED;
};

/**
 * register a facade who'll execute the corresponding business logic for specified resource
 * @param resourceImpl implementation of smFeature, the object that contains the api
 */
smFeatures.register = function (resourceImpl)
{
    smFeatures.resources[resourceImpl.name] = resourceImpl;
    
    return smFeatures;
};
