// TODO add support for html console

var smConsole = new function (){};

smConsole.info = function (message)
{
    console.info(message)
};

smConsole.warn = function (message)
{
    console.warn(message)
};

smConsole.error = function (message)
{
    console.error(message)
};

smConsole.log = function (message)
{
    console.log(message)
};