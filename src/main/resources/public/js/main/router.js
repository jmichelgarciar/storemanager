var smRouter = new function ()
{
    $(window).on('hashchange', function ()
    {
        smRouter.parseUri();
    });
};

smRouter.updateHash = function (hashUri)
{
    if (hashUri.indexOf("#/") != 0)
    {
        smConsole.warn("Cannot load resource, hashUri is not well formatted, hashUri: " + hashUri)
    }
    else
    {
        window.location.hash = hashUri;
    }
};

// uri = http://host:port/#resource/command/base64(json)
smRouter.parseUri = function ()
{
    var hashParts = window.location.hash.replace("#/", "").split("/");

    if (hashParts.length > 3)
    {
        smConsole.error("The uri contains more parts than is supposed, expected: 3, actual: " + hashParts.length
            + ", expected format: #feature/command/base64(json)");

        return;
    }

    var feature  = hashParts.length > 0 ? hashParts[0] : '',
        command  = hashParts.length > 1 ? hashParts[1] : '',
        jsObject = hashParts.length > 2 ? $.parseJSON(atob(hashParts[2])) : {};

    smConsole.info("Detected uri parts, feature: " + feature + ", command: " + command + ", jsObject: " + jsObject);

    if (command != "")
    {
        var executed = smFeatures.execute(feature, command, jsObject);

        if (executed == smFeatures.NOT_EXECUTED)
        {
            smConsole.error("The command has not been executed, feature: " + feature + ", command: " + command)
        }
    }
};