var smFragments = new function (){
    this.fragments = {};
    this.$fragmentContainers = {};
    this.isReady = false;
    this.callbacksCache = [];
};

smFragments.loadFragments = function ()
{
    smFragments.isReady = false;
    
    var $htmlFragments = $('.html-fragment');
    var itemsToLoad = $htmlFragments.length;

    if($htmlFragments.length == 0)
    {
        smFragments.getReady();
    }

    $htmlFragments.each(function()
    {
        var $self = $(this);
        var html = '';
        var url = $self.attr("href");

        simpleCheck(url);

        smFragments.$fragmentContainers[url] = $self;

        $.get(url)
            .done(function(data)
            {
                html = data;
            })
            .fail(function()
            {
                console.log("Error occurred while loading html fragment..");
            })
            .always(function()
            {
                console.log("HTML Fragment loaded, url: " + url);

                smFragments.fragments[url] = html;
                itemsToLoad--;

                if(itemsToLoad < 1)
                {
                    //init already loaded fragments
                    smFragments.initFragments();

                    // load again, to get nested fragments inside imported fragments
                    smFragments.loadFragments();
                }
            })

    });

    function simpleCheck(url)
    {
        if(typeof smFragments.fragments[url] != "undefined")
        {
            console.warn("HTML Fragment url already was registered, url: " + url);
        }

        if(typeof smFragments.$fragmentContainers[url] != "undefined")
        {
            console.warn("Container for fragment url already was registered, url: " + url);
        }
    }
    
    return smFragments;
};

smFragments.initFragments = function()
{
    for(var url in smFragments.fragments)
    {
        smFragments.$fragmentContainers[url].replaceWith(smFragments.fragments[url])
    }
};

smFragments.getReady = function()
{
    //smFragments.initFragments();

    smFragments.isReady = true;

    for(var callbackName in smFragments.callbacksCache)
    {
        var callback = smFragments.callbacksCache[callbackName];

        if(typeof callback == 'function')
        {
            callback.call(smFragments);
        }
        else
        {
            console.warn("Found wrong callback types in fragments handler, callbackName: " + callbackName);
        }
    }

    smFragments.callbacksCache = [];
};

/**
 * call the callback when all fragments are loaded
 * if fragments are already loaded, the callback is called right away
 * 
 * @param callback the function to call
 */
smFragments.whenReady = function(callback)
{
    if(smFragments.isReady)
    {
        callback.call(smFragments);
    }
    else
    {
        if(typeof callback == 'function')
        {
            smFragments.callbacksCache.push(callback);
        }
        else
        {
            console.warn("Trying to register a non-functional callback, callbackName: " + callback);
        }
    }
};

smFragments.dataToHtml = function(data, _template)
{
    var html = '';

    if (typeof data == "string")
    {
        data = JSON.parse(data);
    }

    var nr = data.length;

    for (var i = 0; i < nr; i++)
    {
        var value = data[i];

        html += _template.replace(/#\[[a-z]{1,}\]/gi, function (match)
        {
            match = match.replace("#[", '').replace("]", '');

            if (typeof value[match] != 'undefined')
            {
                return value[match];
            }

            return match;
        })
    }
    return html;
};