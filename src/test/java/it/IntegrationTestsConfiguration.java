package it;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.storemanager")
@TestPropertySource(locations = "classpath:application.yml")
public class IntegrationTestsConfiguration
{

}