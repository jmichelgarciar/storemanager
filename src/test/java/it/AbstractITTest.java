package it;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

/**
 * rodislav
 * 20.02.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {IntegrationTestsConfiguration.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
public abstract class AbstractITTest
{
    public static final String HTTP_LOCALHOST    = "http://localhost:";
    protected RestTemplate restTemplate = new RestTemplate();

    @Value("${local.server.port}")
    private int port;

    private static AbstractITTest instance;

    public AbstractITTest()
    {
        instance = this;
    }

    public int getPort()
    {
        return port;
    }

    public static String getUrl(final String uri)
    {
        return HTTP_LOCALHOST + instance.port + uri;
    }
}
