package it.main.http.v1;

import com.storemanager.features.attribute.MeasureUnit;
import com.storemanager.features.attribute.Type;
import it.AbstractITTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static com.storemanager.main.http.v1.HttpApiConstV1.*;
import static org.junit.Assert.*;

/**
 * rodislav
 * 20.02.16.
 */
public class AttributeControllerV1Test extends AbstractITTest
{
    private static final Logger       LOGGER       = LoggerFactory.getLogger(MainControllerV1Test.class);
    private static       RestTemplate restTemplate = new RestTemplate();

    @Test
    public void testAddAttributes()
    {
//        check MeasureUnit
        MeasureUnit dummy = getDummyMeasureUnit();
        MeasureUnit added = insertMeasureUnit();
        LOGGER.info(String.valueOf(added));

        assertNotEquals(dummy.getId(), added.getId());

//        we compare both products within a generic method, so this set is only a workaround
        dummy.setId(added.getId());
        compare(dummy, added);

//        check Type
        Type dummyType = getDummyType();
        Type addedType = insertType();
        LOGGER.info(String.valueOf(addedType));

        assertNotEquals(dummyType.getId(), addedType.getId());

//        we compare both products within a generic method, so this set is only a workaround
        dummyType.setId(addedType.getId());

        compare(dummyType, addedType);
    }

    @Test
    public void testGetAttributes()
    {
        MeasureUnit addedMeasureUnit = insertMeasureUnit();

        final MeasureUnit gottenMeasureUnit = restTemplate.getForObject(
                getUrl(V1_ATTRIBUTE_MEASUREUNIT) + "/" + addedMeasureUnit.getId(),
                MeasureUnit.class, Collections.emptyMap());

        compare(addedMeasureUnit, gottenMeasureUnit);

        Type addedType = insertType();

        Type gottenType = restTemplate.getForObject(
                getUrl(V1_ATTRIBUTE_TYPE) + "/" + addedType.getId(),
                Type.class, Collections.emptyMap());

        compare(addedType, gottenType);
    }

    @Test
    public void testGetAttributesList()
    {
        insertMeasureUnit();
        insertType();

        List<MeasureUnit> value = restTemplate.exchange(
                getUrl(V1_ATTRIBUTE_MEASUREUNIT_LIST),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<MeasureUnit>>() {}).getBody();

        List<Type> value1 = restTemplate.exchange(
                getUrl(V1_ATTRIBUTE_TYPE_LIST),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Type>>() {}).getBody();

        assertTrue(value.size() > 0);
        assertTrue(value1.size() > 0);
    }

    @Test
    public void testDeleteAttribute()
    {
        MeasureUnit addedMeasureUnit = insertMeasureUnit();
        Type addedType = insertType();

        restTemplate.delete(getUrl(V1_ATTRIBUTE_MEASUREUNIT) + "/" + addedMeasureUnit.getId());
        restTemplate.delete(getUrl(V1_ATTRIBUTE_TYPE) + "/" + addedType.getId());

        MeasureUnit gottenMeasureUnit = restTemplate.getForObject(
                getUrl(V1_ATTRIBUTE_MEASUREUNIT) + "/" + addedMeasureUnit.getId(),
                MeasureUnit.class, Collections.emptyMap());

        assertNull(gottenMeasureUnit);

        Type gottenType = restTemplate.getForObject(
                getUrl(V1_ATTRIBUTE_TYPE) + "/" + addedType.getId(),
                Type.class, Collections.emptyMap());

        assertNull(gottenType);
    }

    public static MeasureUnit insertMeasureUnit()
    {
        final MeasureUnit dummy = getDummyMeasureUnit();

        ResponseEntity<MeasureUnit> responseEntity = restTemplate.postForEntity(
                getUrl(V1_ATTRIBUTE_MEASUREUNIT),
                dummy,
                MeasureUnit.class,
                Collections.emptyMap());

        return responseEntity.getBody();
    }

    public static Type insertType()
    {
        final ResponseEntity<Type> responseEntity = restTemplate.postForEntity(
                getUrl(V1_ATTRIBUTE_TYPE),
                getDummyType(),
                Type.class,
                Collections.emptyMap());

        return responseEntity.getBody();
    }

    //        TODO add reflections utils to compare all fields in generic way, or a utility function to compare 2 objects
//        todo maybe use some libs to accomplish that, like BeanUtils
    private void compare(final MeasureUnit expected, final MeasureUnit actual)
    {
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getShortDescription(), actual.getShortDescription());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getId(), actual.getId());
    }

    private void compare(final Type expected, final Type actual)
    {
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getShortDescription(), actual.getShortDescription());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getId(), actual.getId());
    }

    private static MeasureUnit getDummyMeasureUnit()
    {
        final MeasureUnit value = new MeasureUnit();
        value.setId(-1);
        value.setName("measure unit");
        value.setDescription("description");
        value.setShortDescription("short description");

        return value;
    }

    private static Type getDummyType()
    {
        final Type value = new Type();
        value.setId(-1);
        value.setName("type");
        value.setDescription("description");
        value.setShortDescription("short description");

        return value;
    }

}
