package it.main.http.v1;

import com.storemanager.main.i18n.LocaleMessageService;
import it.AbstractITTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import static com.storemanager.main.http.v1.HttpApiConstV1.V1_STATUS;
import static com.storemanager.main.http.v1.HttpApiConstV1.V1_SWAGGER;
import static org.junit.Assert.*;

/**
 * rodislav
 * 03.02.16.
 */
public class MainControllerV1Test extends AbstractITTest
{

    @Autowired
    private LocaleMessageService messageService;

    private static final Logger LOGGER = LoggerFactory.getLogger(MainControllerV1Test.class);

    @Test
    public void checkStatusTest() throws InterruptedException
    {
        RestTemplate restTemplate = new RestTemplate();
        String statusMessage = restTemplate.getForObject(getUrl(V1_STATUS), String.class);

        assertEquals(messageService.getMessage("v1.status"), statusMessage);
    }

    @Test
    public void checkSwaggerTest() throws InterruptedException
    {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(getUrl(V1_SWAGGER), String.class);

        assertNotNull(result);
        assertNotEquals("", result);
        assertTrue(result.contains("<title>Swagger UI</title>"));
    }

}
