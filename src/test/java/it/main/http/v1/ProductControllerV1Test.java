package it.main.http.v1;

import com.storemanager.features.attribute.MeasureUnit;
import com.storemanager.features.attribute.Type;
import com.storemanager.features.product.Product;
import it.AbstractITTest;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;

import java.util.Collections;
import java.util.List;

import static com.storemanager.main.http.v1.HttpApiConstV1.V1_PRODUCT;
import static com.storemanager.main.http.v1.HttpApiConstV1.V1_PRODUCT_LIST;
import static org.junit.Assert.*;

/**
 * rodislav
 * 20.02.16.
 */
public class ProductControllerV1Test extends AbstractITTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MainControllerV1Test.class);

    private Product insertProduct(final Product dummy)
    {
        return restTemplate.postForEntity(getUrl(V1_PRODUCT),
                dummy, Product.class, Collections.emptyMap()).getBody();
    }

    @Test
    public void addProductTest()
    {
        MeasureUnit measureUnit = AttributeControllerV1Test.insertMeasureUnit();
        Type type = AttributeControllerV1Test.insertType();

        final Product dummy = getDummyProduct(measureUnit.getId(), type.getId());
        Product addedProduct = insertProduct(dummy);

        LOGGER.info(String.valueOf(addedProduct));

        assertNotEquals(dummy.getId(), addedProduct.getId());

//        we compare both products within a generic method, so this set is only a workaround
        dummy.setId(addedProduct.getId());
        compareProducts(dummy, addedProduct);
    }

    @Test
    public void getProductTest()
    {
        MeasureUnit measureUnit = AttributeControllerV1Test.insertMeasureUnit();
        Type type = AttributeControllerV1Test.insertType();

        final Product dummy = getDummyProduct(measureUnit.getId(), type.getId());
        Product addedProduct = insertProduct(dummy);

        final Product receivedProduct = restTemplate.getForObject(getUrl(V1_PRODUCT) + "/" + addedProduct.getId(),
                Product.class,
                Collections.emptyMap());

        compareProducts(addedProduct, receivedProduct);
    }

    @Test
    public void getProductListTest()
    {
        MeasureUnit measureUnit = AttributeControllerV1Test.insertMeasureUnit();
        Type type = AttributeControllerV1Test.insertType();

        final Product dummy = getDummyProduct(measureUnit.getId(), type.getId());
        insertProduct(dummy);

        final List<Product> receivedProducts = restTemplate.exchange(getUrl(V1_PRODUCT_LIST),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>(){}).getBody();

        assertTrue(receivedProducts.size() > 0);
    }

    @Test
    public void deleteProductTest()
    {
        MeasureUnit measureUnit = AttributeControllerV1Test.insertMeasureUnit();
        Type type = AttributeControllerV1Test.insertType();

        final Product dummy = getDummyProduct(measureUnit.getId(), type.getId());
        Product addedProduct = insertProduct(dummy);

        restTemplate.delete(getUrl(V1_PRODUCT) + "/" + addedProduct.getId());

        final Product receivedProduct = restTemplate.getForObject(getUrl(V1_PRODUCT) + "/" + addedProduct.getId(),
                Product.class,
                Collections.emptyMap());

        assertNull(receivedProduct);
    }

//        TODO add reflections utils to compare all fields in generic way, or a utility function to compare 2 objects
//        todo maybe use some libs to accomplish that, like BeanUtils
    private void compareProducts(final Product expected, final Product actual)
    {
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getShortDescription(), actual.getShortDescription());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getMeasureUnitId(), actual.getMeasureUnitId());
        assertEquals(expected.getTypeId(), actual.getTypeId());
        assertEquals(expected.getId(), actual.getId());
    }

    private Product getDummyProduct(Long measureUnitId, Long typeId)
    {
        final Product dummy = new Product();
        dummy.setId(-1);
        dummy.setName("dummy product");
        dummy.setDescription("description");
        dummy.setShortDescription("short description");
        dummy.setMeasureUnitId(measureUnitId);
        dummy.setTypeId(typeId);

        return dummy;
    }

}
