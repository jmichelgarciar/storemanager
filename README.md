# README #

StoreManager is an application to manage a store, the main features are manage the: assets, products, workers, etc.
Licensed under MIT License, please check the LICENSE.lic file for details

* Quick summary
* 0.0.1-SNAPSHOT
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* Requirements
Java 8, Maven 3.LATEST

* Configuration
You can change *application.yml* files within the *src/main/resources* to adapt the application to your environment

* Dependencies
Please use *maven* to download all dependencies

* Database configuration
By default HSQLDB is used, you can configure it the way you want
 examples to run it you'll find in application.yml files from sources
 also you have an \*.sh script to run HSQLDB as a service 

* How to run tests
*mvn verify* should do the trick

* Deployment instructions
For now you can use maven with spring-boot plugin to run it
when the app will reach the release version, it'll get a runner for Linux, Windows and Mac

### Contribution guidelines ###

* Writing tests
the development is TDD/BDD oriented, so any new feature should start from a test (*unit or integration*) who'll test the use case
the feature is considered stable and can be merged only if all tests are green!

* Code review
Code review will be done for each pull request and will pass sonar check

* Other guidelines
Please respect the naming and packages structure

### Who do I talk to? ###

* Repo owner or admin